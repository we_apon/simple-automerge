﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Runtime.CompilerServices;
using Ninject;
using SimpleAutoMerge.Abstract.Services;

namespace SimpleAutoMerge.Services
{
    public class NinjectContainer : IDIContainer
    {
        private readonly IKernel _kernel;

        public NinjectContainer(IKernel kernel) {
            Contract.Requires<ArgumentNullException>(kernel != null);
            _kernel = kernel;
        }


        public T Get<T>() {
            return _kernel.Get<T>();
        }

        public IEnumerable<T> GetAll<T>() {
            return _kernel.GetAll<T>();
        }
    }
}