﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using JetBrains.Annotations;
using SimpleAutoMerge.Abstract.Services;
using SimpleAutoMerge.Abstract.Structures;
using SimpleAutoMerge.Structures;

namespace SimpleAutoMerge.Services.Common
{  
    public class TextMerger : ITextMerger {
        public IMergeResult Merge([NotNull] IProcessingFiles files) {
            Contract.Requires<ArgumentNullException>(files != null);
            Contract.Requires<ArgumentException>(files.SourceFile != null);
            Contract.Requires<ArgumentException>(files.ChangedFiles.Any());

            var source = files.SourceFile;

            if (files.ChangedFiles.Select(x => x.Text).All(x => x.SequenceEqual(source.Text))) {
                // I'm copying values, to ensure, that changes to source somewhere out there will not broke merge results
                var output = new TextFile(source.Filename, source.Text.ToList()); 
                return new MergeResult(output);
            }


            //get all modifications
            var mods = new List<ModificationInfo>();
            foreach (var changedFile in files.ChangedFiles) {
                mods.AddRange(source.ModificationsTo(changedFile));
            }

            var outText = mods.MakeText();
            return new MergeResult(new TextFile(source.Filename, outText));
        }
        
    }
}