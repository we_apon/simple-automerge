using System;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Text;
using SimpleAutoMerge.Abstract.Services;
using SimpleAutoMerge.Abstract.Structures;
using SimpleAutoMerge.Structures;

namespace SimpleAutoMerge.Services.Common
{
    public class DefaultAppArgsInterpreter: IAppArgsInterpreter {
        public string Help { get { return print_help(); } }


        public bool CanParse(string[] args) {
            Contract.Requires<ArgumentNullException>(args != null);
            if (args.Count() < 2)
                return false;

            return args.All(isFilename);
        }

        public IAppParameters Parse(string[] args) {
            if (!CanParse(args))
                throw new ArgumentException("Unknown arguments format");

            return new AppParameters(args.First(), args.Skip(1).ToArray());
        }



        private bool isFilename(string path) {
            return path.IndexOfAny(Path.GetInvalidPathChars()) < 0;
        }



        private static string print_help() {
            var text = new StringBuilder();
            text.AppendLine("\n-----------------------");
            text.AppendLine("Default SimpleAutoMerge syntax");
            text.AppendLine("-------------------------------------\n");
            text.AppendLine("SimpleAutoMerge.exe 'source.file' 'changed-one.file' [changed-two.file] [changed-N.files]\n");
            text.AppendLine("![CAUTION] With this syntax 'source.file' will be overwritten!\n\n\n");
            return text.ToString();
        }
    }
}