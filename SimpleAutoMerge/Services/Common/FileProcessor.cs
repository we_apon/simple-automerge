using System;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using SimpleAutoMerge.Abstract.Services;
using SimpleAutoMerge.Abstract.Structures;
using SimpleAutoMerge.Structures;

namespace SimpleAutoMerge.Services.Common
{
    public class FileProcessor : IFileProcessor {
        public IProcessingFiles LoadFrom([NotNull] IAppParameters parameters) {
            Contract.Requires<ArgumentNullException>(parameters != null);
            Contract.Requires<ArgumentException>(!string.IsNullOrWhiteSpace(parameters.SourceFilename));
            Contract.Requires<ArgumentException>(parameters.ChangedFilenames.Any());

            
            if (!File.Exists(parameters.SourceFilename))
                throw new FileNotFoundException("Source file was not founded", parameters.SourceFilename);

            var sourceFile = new TextFile(parameters.SourceFilename, File.ReadAllLines(parameters.SourceFilename));
            var processingFiles = new ProcessingFiles(sourceFile);


            foreach (var filename in parameters.ChangedFilenames) {
                if (!File.Exists(filename))
                    throw new FileNotFoundException("Changed file was not founded", filename);

                var changedFile = new TextFile(filename, File.ReadAllLines(filename));
                processingFiles.ChangedFiles.Add(changedFile);
            }

            return processingFiles;
        }


        public void Write(ITextFile file) {
            File.WriteAllLines(file.Filename, file.Text);
        }
    }
}