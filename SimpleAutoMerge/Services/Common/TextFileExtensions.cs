using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using SimpleAutoMerge.Abstract.Structures;
using SimpleAutoMerge.Structures;

namespace SimpleAutoMerge.Services.Common
{
    internal static class TextFileExtensions
    {
        [CanBeNull] public static string LineAt(this ITextFile file, int idx, bool trim = true) {
            return file.Text.Count > idx ? (trim ? file.Text[idx].Trim() : file.Text[idx]) : null;
        }


        public static IList<ModificationInfo> ModificationsTo(this ITextFile source, ITextFile changed)
        {
            var sourceIdx = 0;
            var changedIdx = 0;

            var modifications = new List<ModificationInfo>();
            var sourceCount = source.Text.Count;
            var changedCount = changed.Text.Count;

            while (sourceIdx < sourceCount || changedIdx < changedCount) {
                var modification = source.GetModificationOfLine(sourceIdx, changed, changedIdx);
                modifications.Add(modification);

                if (modification.Type != ChangeType.Inserted)
                    sourceIdx++;

                changedIdx += modification.ChangindLength;
            }

            return modifications;
        }


        public static ModificationInfo GetModificationOfLine(this ITextFile source, int sourceIdx, ITextFile changed, int changedIdx)
        {

            var modification = new ModificationInfo {
                Source = source,
                Changed = changed,
                SourceIdx = sourceIdx < source.Text.Count ? sourceIdx : source.Text.Count - 1,
                ChangingIdx = changedIdx,
                ChangindLength = 1
            };

            var sourceLine = source.LineAt(sourceIdx);
            var changedLine = changed.LineAt(changedIdx);


            if (sourceLine != null && changedLine != null && sourceLine.Equals(changedLine)) {
                modification.Type = ChangeType.Nothing;
                return modification;
            }

            if (source.is_row_just_changed(sourceIdx, changed, changedIdx)) { 
                modification.Type = ChangeType.Changed;
                return modification;
            }


            if (source.is_row_inserted(sourceIdx, changed, changedIdx)) {
                modification.Type = sourceIdx < source.Text.Count ? ChangeType.Inserted : ChangeType.Appended;
                modification.ChangindLength = sourceLine != null 
                    ? changed.IndexOfLineFromIdx(sourceLine, changedIdx) 
                    : changed.Text.Count - changedIdx;

                return modification;
            }

            
            // line was deleted
            modification.Type = ChangeType.Deleted;
            modification.ChangindLength = 0;
            return modification;
        }




        public static int IndexOfLineFromIdx(this ITextFile file, string line, int idx) {
            if (file.Text.Count <= idx)
                return -1;

            var i = -1;
            var first = file.Text.Skip(idx).FirstOrDefault(x => {
                i++;
                return x.Trim().Equals(line);
            });

            return first != null ? i : -1;
        }


        public static bool IsHaveLine(this ITextFile file, string line, int idx) {
            return Line(file, line, idx) != null;
        }


        [CanBeNull] public static string Line(this ITextFile file, string line, int idx) {
            return file.Text.Skip(idx).FirstOrDefault(x => x.Trim().Equals(line));
        }


        private static bool is_row_just_changed(this ITextFile source, int sourceIdx, ITextFile changed, int changedIdx)
        {
            var sourceLine = source.LineAt(sourceIdx);
            if (sourceLine == null)
                return false; // cause it's seems to be insertion

            
            var changedLine = changed.LineAt(changedIdx);
            if (changedLine == null)
                return false; // cause it's seems to be deletion

            return !source.IsHaveLine(changedLine, sourceIdx) && !changed.IsHaveLine(sourceLine, changedIdx);
        }
        

        private static bool is_row_inserted(this ITextFile source, int sourceIdx, ITextFile changed, int changedIdx) {
            var sourceLine = source.LineAt(sourceIdx);
            return sourceLine == null || changed.IsHaveLine(sourceLine, changedIdx);
        }
    }
}