using System;
using System.Diagnostics.Contracts;
using System.Linq;
using JetBrains.Annotations;
using SimpleAutoMerge.Abstract.Services;

namespace SimpleAutoMerge.Services.Common
{
    public class ArgsInterpreterFactory : IArgsInterpreterFactory {

        public ArgsInterpreterFactory([NotNull] IAppArgsInterpreter[] interpreters) {
            Contract.Requires<ArgumentNullException>(interpreters != null);
            Contract.Requires<ArgumentException>(interpreters.Any());

            _interpreters = interpreters;
        }


        public IAppArgsInterpreter InterpreterFor(string[] args) {
            return _interpreters.FirstOrDefault(x => x.CanParse(args));
        }


        private readonly IAppArgsInterpreter[] _interpreters;
    }
}