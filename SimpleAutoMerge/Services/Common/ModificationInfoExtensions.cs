using System.Collections.Generic;
using System.Linq;
using SimpleAutoMerge.Structures;

namespace SimpleAutoMerge.Services.Common
{
    internal static class ModificationInfoExtensions
    {
        public static IList<string> MakeText(this IList<ModificationInfo> info) {
            var source = info.Select(x => x.Source).First(x => x != null);

            var text = new List<string>();

            var idx = -1;
            while (++idx < source.Text.Count) {
                var modifications = info.Where(x => x.SourceIdx == idx).ToList();
                var types = modifications.Select(x => x.Type).Distinct().ToList();


                if (types.Count > 1) {
                    var appendix = modifications.Where(x => x.Type == ChangeType.Appended).ToList();
                    types.Remove(ChangeType.Appended);

                    if (types.Any(x => x == ChangeType.Inserted)) {
                        var insertions = modifications.Where(x => x.Type == ChangeType.Inserted).ToList();
                        text.AddRange(insertions.all_text());
                        types.Remove(ChangeType.Inserted);
                        types.Remove(ChangeType.Nothing); //bug ?

                        modifications.RemoveAll(insertions.Contains);
                    }

                    if (!types.Any() || (types.Count == 1 && types.Single() == ChangeType.Nothing)) {
                        // insert source text after insertions
                        text.Add(source.LineAt(idx, trim: false));
                        continue;
                    }

                    if (types.Count > 1 && types.Contains(ChangeType.Nothing))
                        types.Remove(ChangeType.Nothing);


                    if (types.Count > 1) {
                        text.AddRange(modifications.conflict());
                        continue;
                    }

                    if (types.Single() == ChangeType.Deleted) {
                        continue;
                    }

                    //else - Changing
                    var changings = modifications.Where(x => x.Type == ChangeType.Changed).ToList();
                    text.AddRange(changings.all_text());

                    if (appendix.Any())
                        text.AddRange(appendix.all_text());
                    continue;
                }


                switch (types.Single()) {
                    case ChangeType.Nothing: text.Add(source.LineAt(idx, trim: false)); break;
                    case ChangeType.Deleted: break;
                    case ChangeType.Changed: 
                        if (modifications.Count > 1) {
                            text.AddRange(modifications.conflict());
                            continue;
                        }
                        text.AddRange(modifications.all_text());
                        break;

                    case ChangeType.Inserted:
                        text.AddRange(modifications.all_text());
                        text.Add(source.LineAt(idx, trim: false));
                        break;
                }
            }

            return text;
        }


        private static IEnumerable<string> conflict<T>(this IList<T> modifications) where T : ModificationInfo {
            var text = new List<string>();

            var source = modifications.First().Source;

            text.Add("//// Conflict ////");
            text.Add(string.Format("//// {0}:", source.Filename));
            text.Add(source.Text[modifications.First().SourceIdx]);

            var files = modifications.Select(x => x.Changed).Distinct();

            foreach (var file in files)
            {
                text.Add(string.Format("//// {0}:", file.Filename));
                foreach (var modification in modifications.Where(x => x.Changed == file).ToList()) {
                    switch (modification.Type) {
                        case ChangeType.Deleted: text.Add("//// line was deleted"); break;
                        case ChangeType.Inserted: text.AddRange(modification.Text()); break; // this never should be happen
                        case ChangeType.Changed: text.AddRange(modification.Text()); break;
                    }
                }
            }

            text.Add("//// End of conflict ////");
            return text;
        }


        private static IEnumerable<string> all_text<T>(this IEnumerable<T> modifications) where T : ModificationInfo {
            var text = new List<string>();

            foreach (var modification in modifications) {
                text.AddRange(modification.Text());
            }

            return text;
        }
    }
}