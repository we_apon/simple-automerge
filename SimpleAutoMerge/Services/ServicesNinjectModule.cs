using Ninject.Modules;
using SimpleAutoMerge.Abstract.Services;
using SimpleAutoMerge.Services.Common;

namespace SimpleAutoMerge.Services
{
    public class ServicesNinjectModule : NinjectModule {
        public override void Load() {
            Bind<IAppArgsInterpreter>().To<DefaultAppArgsInterpreter>();
            Bind<IArgsInterpreterFactory>().To<ArgsInterpreterFactory>();
            Bind<IFileProcessor>().To<FileProcessor>();
            Bind<ITextMerger>().To<TextMerger>();
        }
    }
}