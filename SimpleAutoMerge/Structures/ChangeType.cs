namespace SimpleAutoMerge.Structures
{
    public enum ChangeType
    {
        Nothing,
        Deleted,
        Inserted,
        Changed,
        Appended
    }
}