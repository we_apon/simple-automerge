using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using JetBrains.Annotations;
using SimpleAutoMerge.Abstract.Structures;

namespace SimpleAutoMerge.Structures
{
    public class ProcessingFiles : IProcessingFiles
    {
        public ProcessingFiles([NotNull] ITextFile source) {
            Contract.Requires<ArgumentNullException>(source != null);
            SourceFile = source;
            ChangedFiles = new List<ITextFile>();
        }

        public ITextFile SourceFile { get; private set; }
        public IList<ITextFile> ChangedFiles { get; private set; }
    }
}