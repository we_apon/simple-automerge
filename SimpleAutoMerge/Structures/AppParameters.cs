using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using SimpleAutoMerge.Abstract.Structures;

namespace SimpleAutoMerge.Structures
{
    public class AppParameters : IAppParameters
    {
        public AppParameters(string source, IList<string> changed) {
            Contract.Requires<ArgumentNullException>(source != null);
            Contract.Requires<ArgumentNullException>(changed != null);

            SourceFilename = source;
            ChangedFilenames = changed;
        }

        public string SourceFilename { get; private set; }
        public IList<string> ChangedFilenames { get; private set; }
    }
}