using System;
using System.Diagnostics.Contracts;
using JetBrains.Annotations;
using SimpleAutoMerge.Abstract.Services;
using SimpleAutoMerge.Abstract.Structures;

namespace SimpleAutoMerge.Structures
{
    public class MergeResult : IMergeResult
    {
        public MergeResult([NotNull] ITextFile outputFile) {
            Contract.Requires<ArgumentNullException>(outputFile != null);
            Output = outputFile;
        }

        public ITextFile Output { get; private set; }
    }
}