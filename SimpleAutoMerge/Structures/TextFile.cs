using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using SimpleAutoMerge.Abstract.Structures;

namespace SimpleAutoMerge.Structures
{
    public class TextFile : ITextFile
    {
        public TextFile(string filename, IList<string> text) {
            Contract.Requires<ArgumentNullException>(filename != null);
            Contract.Requires<ArgumentNullException>(text != null);

            Filename = filename;
            Text = text;
        }

        public string Filename { get; private set; }
        public IList<string> Text { get; private set; }
    }
}