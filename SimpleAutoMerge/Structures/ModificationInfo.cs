using System.Collections.Generic;
using System.Linq;
using SimpleAutoMerge.Abstract.Structures;
using SimpleAutoMerge.Services.Common;

namespace SimpleAutoMerge.Structures
{
    public class ModificationInfo
    {
        public int SourceIdx { get; set; }
        public int ChangingIdx { get; set; }
        public int ChangindLength { get; set; }
        public ChangeType Type { get; set; }

        public ITextFile Source { get; set; }
        public ITextFile Changed { get; set; }

        public IList<string> Text()
        {
            var list = new List<string>();
            switch (Type)
            {
                case ChangeType.Nothing: list.Add(Source.LineAt(SourceIdx, trim: false)); break;
                case ChangeType.Deleted: break;
                case ChangeType.Changed: list.Add(Changed.LineAt(ChangingIdx, trim: false)); break;
                case ChangeType.Inserted:
                case ChangeType.Appended:
                    var i = ChangingIdx;
                    while (i < ChangingIdx + ChangindLength)
                    {
                        list.Add(Changed.LineAt(i++, trim: false));
                    }
                    break;
            }
            return list;
        }

        public override string ToString() {
            return string.Format("{0}: {1}", Type, Text().Aggregate(string.Empty, (current, s) => current + s + "\n"));
        }
    }
}