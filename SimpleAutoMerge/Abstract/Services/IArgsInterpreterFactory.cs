﻿using JetBrains.Annotations;

namespace SimpleAutoMerge.Abstract.Services
{
    public interface IArgsInterpreterFactory
    {
        /// <summary> Returns an interpreter for input args. </summary>
        [CanBeNull] IAppArgsInterpreter InterpreterFor([NotNull] string[] args);
    }
}
