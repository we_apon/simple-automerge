﻿using JetBrains.Annotations;
using SimpleAutoMerge.Abstract.Structures;

namespace SimpleAutoMerge.Abstract.Services
{
    public interface IFileProcessor
    {
        [NotNull] IProcessingFiles LoadFrom(IAppParameters parameters);

        void Write([NotNull] ITextFile file);
    }
}
