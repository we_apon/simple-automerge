﻿using JetBrains.Annotations;
using SimpleAutoMerge.Abstract.Structures;

namespace SimpleAutoMerge.Abstract.Services
{
    public interface ITextMerger
    {
        [NotNull] IMergeResult Merge(IProcessingFiles files);
    }


    public interface IMergeResult
    {
        [NotNull] ITextFile Output { get; }
    }

}
