﻿using JetBrains.Annotations;
using SimpleAutoMerge.Abstract.Structures;

namespace SimpleAutoMerge.Abstract.Services
{
    public interface IAppArgsInterpreter
    {
        string Help { get; }
        bool CanParse(string[] args);

        [NotNull] IAppParameters Parse(string[] args);
    }
}
