using System.Collections.Generic;
using JetBrains.Annotations;

namespace SimpleAutoMerge.Abstract.Services
{
    public interface IDIContainer
    {
        [NotNull] T Get<T>();
        [NotNull] IEnumerable<T> GetAll<T>();
    }
}