﻿using System.Collections.Generic;

namespace SimpleAutoMerge.Abstract.Structures
{
    public interface IAppParameters
    {
        string SourceFilename { get; }
        IList<string> ChangedFilenames { get; } 
    }
}