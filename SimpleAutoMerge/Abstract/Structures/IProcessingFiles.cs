﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace SimpleAutoMerge.Abstract.Structures
{
    public interface IProcessingFiles
    {
        [NotNull] ITextFile SourceFile { get; }
        [NotNull] IList<ITextFile> ChangedFiles { get; }
    }
}
