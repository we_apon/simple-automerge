﻿using System.Collections.Generic;

namespace SimpleAutoMerge.Abstract.Structures
{
    public interface ITextFile
    {
        string Filename { get; }
        IList<string> Text { get; }
    }

}
