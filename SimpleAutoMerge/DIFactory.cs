﻿using Ninject;
using SimpleAutoMerge.Abstract.Services;
using SimpleAutoMerge.Services;

namespace SimpleAutoMerge
{
    public class DIFactory
    {
        public static IDIContainer GetContainer() { // Да, я привык абстрагигироваться даже от реализации DI-контейнера
            var kernel = new StandardKernel(new ServicesNinjectModule());
            return new NinjectContainer(kernel);
        }
    }
}
