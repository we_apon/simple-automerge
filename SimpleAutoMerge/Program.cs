﻿using System;
using System.Linq;
using SimpleAutoMerge.Abstract.Services;

namespace SimpleAutoMerge
{
    class Program
    {
        private static IDIContainer _container;
        public static IDIContainer Container {
            get {
                if (_container == null)
                    _container = DIFactory.GetContainer();
                
                return _container;
            }
        }

        public static IFileProcessor FileProcessor { get { return Container.Get<IFileProcessor>(); } }
        public static ITextMerger Merger { get { return Container.Get<ITextMerger>(); } }
        public static IArgsInterpreterFactory InterpreterFactory { get { return Container.Get<IArgsInterpreterFactory>(); } } 


        static void Main(string[] args) {
            var interpreter = InterpreterFactory.InterpreterFor(args);
            if (interpreter == null) {
                foreach (var i in Container.GetAll<IAppArgsInterpreter>()) {
                    Console.WriteLine(i.Help);
                }
                return;
            }

            var parameters = interpreter.Parse(args);
            var files = FileProcessor.LoadFrom(parameters);
            var result = Merger.Merge(files);

            FileProcessor.Write(result.Output);

        }
    }
}
