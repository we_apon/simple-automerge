﻿using System.IO;
using System.Linq;
using Moq;
using NBehave.Spec.Xunit;
using SimpleAutoMerge.Abstract.Structures;
using Xunit;

// ReSharper disable once CheckNamespace
namespace SimpleAutoMerge.Services.Common.When_Using_File_Processor
{
    public class When_Using_File_Processor : Specification
    {
        protected FileProcessor _Processor;
        protected Mock<IAppParameters> _Parameters;

        protected override void Establish_context() {
            base.Establish_context();

            _Parameters = new Mock<IAppParameters>();
            _Parameters.SetupGet(x => x.SourceFilename).Returns(@"Content\Source.txt");
            _Parameters.SetupGet(x => x.ChangedFilenames).Returns(new[] {
                @"Content\ChangedOne.txt",
                @"Content\ChangedTwo.txt"
            });

            _Processor = new FileProcessor();
        }
    }


    public class And_Called_LoadFrom_Method : When_Using_File_Processor
    {
        private IProcessingFiles _files;

        protected override void Because_of() {
            base.Because_of();
            _files = _Processor.LoadFrom(_Parameters.Object);
        }

        [Fact] public void Source_File_Should_Be_Loaded() {
            _files.SourceFile.Filename.ShouldEqual(_Parameters.Object.SourceFilename);
            _files.SourceFile.Text.ShouldEqual(File.ReadAllLines(_Parameters.Object.SourceFilename));
        }

        [Fact] public void All_Changed_Files_Should_Be_Loaded() {
            _files.ChangedFiles.Count.ShouldEqual(_Parameters.Object.ChangedFilenames.Count);

            foreach (var filename in _Parameters.Object.ChangedFilenames) {
                var file = _files.ChangedFiles.Single(x => x.Filename.Equals(filename));
                file.ShouldNotBeNull();
                file.Text.ShouldEqual(File.ReadAllLines(filename));
            }
        }
    }


}
