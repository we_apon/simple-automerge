﻿using Moq;
using NBehave.Spec.Xunit;
using SimpleAutoMerge.Abstract.Structures;
using Xunit;

// ReSharper disable once CheckNamespace
namespace SimpleAutoMerge.Services.Common.When_Using_Default_App_Args_Interpreter
{
    public class When_Using_Default_App_Args_Interpreter : Specification
    {
        protected DefaultAppArgsInterpreter _Interpreter;
        protected string[] _Args;

        protected override void Establish_context() {
            base.Establish_context();
            _Interpreter = new DefaultAppArgsInterpreter();

            _Args = new[] { // To simplified, It will be just a sequence of filenames. First parameter will be the source, other will be the changed files
                "source.cpp",
                @"somefolder\changed_one.cpp",
                @"c:\changed_two.cpp"
            };
        }
    }


    public class And_It_Just_Initialized : When_Using_Default_App_Args_Interpreter
    {
        [Fact] public void It_Should_Be_Abble_To_Interpret_Arguments_In_Default_Format() {
            _Interpreter.CanParse(_Args).ShouldBeTrue();
        }
    }


    public class And_Tell_Him_To_Parse_Supported_Arguments : When_Using_Default_App_Args_Interpreter
    {
        private Mock<IAppParameters> _expectedParameters;
        private IAppParameters _resultParameters;

        protected override void Establish_context() {
            base.Establish_context();
            _expectedParameters = new Mock<IAppParameters>();
            _expectedParameters.SetupGet(x => x.SourceFilename).Returns("source.cpp");
            _expectedParameters.SetupGet(x => x.ChangedFilenames).Returns(new[] { @"somefolder\changed_one.cpp", @"c:\changed_two.cpp" });
        }

        protected override void Because_of() {
            base.Because_of();
            _resultParameters = _Interpreter.Parse(_Args);
        }

        [Fact] public void Valid_App_Parameters_Should_Be_Returned() {
            _resultParameters.SourceFilename.ShouldEqual(_expectedParameters.Object.SourceFilename);
            _resultParameters.ChangedFilenames.ShouldEqual(_expectedParameters.Object.ChangedFilenames);
        }
    }
}
