﻿using Moq;
using NBehave.Spec.Xunit;
using SimpleAutoMerge.Abstract.Services;
using Xunit;

// ReSharper disable once CheckNamespace


namespace SimpleAutoMerge.Services.Common.When_Used_Args_Interpreter_Factory
{
    public class When_Used_Args_Interpreter_Factory : Specification
    {
        protected IAppArgsInterpreter[] _Iterpreters;
        protected ArgsInterpreterFactory _Factory;
        protected string[] _KnownArgs;
        protected Mock<IAppArgsInterpreter> _ExpectedInterpreter;

        protected override void Establish_context() {
            base.Establish_context();

            _KnownArgs = new[] { "some", "args", "of", "known", "format" };

            _ExpectedInterpreter = new Mock<IAppArgsInterpreter>();
            _ExpectedInterpreter.Setup(x => x.CanParse(_KnownArgs)).Returns(true);

            _Iterpreters = new[] {
                new Mock<IAppArgsInterpreter>().Object,
                new Mock<IAppArgsInterpreter>().Object,
                _ExpectedInterpreter.Object,
                new Mock<IAppArgsInterpreter>().Object
            };

            _Factory = new ArgsInterpreterFactory(_Iterpreters);
        }
    }


    public class And_Tell_Him_To_Get_Interpreter_For_Known_Args_Format : When_Used_Args_Interpreter_Factory
    {
        private IAppArgsInterpreter _interpreter;

        protected override void Because_of() {
            base.Because_of();
            _interpreter = _Factory.InterpreterFor(_KnownArgs);
        }

        [Fact] public void Valid_Interpreter_Should_Be_Rerurned() {
            _interpreter.ShouldBeTheSameAs(_ExpectedInterpreter.Object);
        }
    }
}
