﻿using System.Collections.Generic;
using System.Linq;
using NBehave.Spec.Xunit;
using SimpleAutoMerge.Abstract.Services;
using SimpleAutoMerge.Structures;
using Xunit;

// ReSharper disable once CheckNamespace
namespace SimpleAutoMerge.Services.Common.When_Using_Text_Merger
{
    public class When_Using_Text_Merger : Specification
    {
        protected TextMerger _Merger;
        protected List<string> _SourceText;
        protected ProcessingFiles _ProcessingFiles;

        protected override void Establish_context() {
            base.Establish_context();
            _Merger = new TextMerger();

            _SourceText = new List<string> {
                "One",
                "    Two; ",
                "Three",
                "Four"
            };

            var sourceFile = new TextFile("source.txt", _SourceText);
            _ProcessingFiles = new ProcessingFiles(sourceFile);
        }
    }



    public class And_We_Tell_It_To_Merge_Texts_Without_Changes : When_Using_Text_Merger
    {
        private IMergeResult _mergeResult;

        protected override void Establish_context() {
            base.Establish_context();
            _ProcessingFiles.ChangedFiles.Add(new TextFile("one changed file", _SourceText.ToList()));
            _ProcessingFiles.ChangedFiles.Add(new TextFile("another changed file", _SourceText.ToList()));
        }


        protected override void Because_of() {
            base.Because_of();
            _mergeResult = _Merger.Merge(_ProcessingFiles);
        }


        [Fact] public void Resulted_Text_Should_Equals_To_Source() {
            _mergeResult.Output.Text.ShouldEqual(_SourceText);
        }


        [Fact] public void Resulted_Filename_Should_Equals_To_Source_Filename() { // todo: maybe this need to be changed?
            _mergeResult.Output.Filename.ShouldEqual(_ProcessingFiles.SourceFile.Filename); 
        }
        
    }



    public class And_We_Tell_It_To_Merge_Texts_With_Changes_And_Insertions_Without_Conflicts : When_Using_Text_Merger
    {
        private List<string> _expectedText;
        private IMergeResult _mergeResult;

        protected override void Establish_context() {
            base.Establish_context();

            var changedOne = new List<string> {
                "1: One",
                "   2 Two; ",
                "Three",
                "Three and four tens",
                "Four and seven elevens"
            };

            var changedAnother = new List<string> {
                "Zero", 
                "  One ", // should be equal (by specs), and modification in first file - accepted
                "    Two; ",
                " 3",
                "Three and six sevens",
                " Four  ", // should be equal (by spect), and deletion in firls file - accepted
                "Five"
            };


            _expectedText = new List<string> {
                "Zero",
                "1: One",
                "   2 Two; ",
                " 3",
                "Three and six sevens",
                "Three and four tens",
                "Four and seven elevens",
                "Five"
            };


            _ProcessingFiles.ChangedFiles.Add(new TextFile("one changed file", changedOne));
            _ProcessingFiles.ChangedFiles.Add(new TextFile("another changed file", changedAnother));
        }

        protected override void Because_of() {
            base.Because_of();
            _mergeResult = _Merger.Merge(_ProcessingFiles);
        }


        [Fact] public void Merged_Text_Should_Contain_All_Changes_In_Right_Order() {
            _mergeResult.Output.Text.ShouldEqual(_expectedText);
        }


        [Fact] public void Resulted_Filename_Should_Equals_To_Source_Filename() { // todo: maybe this need to be changed?
            _mergeResult.Output.Filename.ShouldEqual(_ProcessingFiles.SourceFile.Filename); 
        }
        
    }


    public class And_We_Tell_It_To_Merge_Texts_With_Changes_And_Conflicts : When_Using_Text_Merger
    {
        private IMergeResult _mergeResult;
        private List<string> _expectedText;


        protected override void Establish_context() {
            base.Establish_context();

            var changedOne = new List<string> {
                "One;",
                " Two; ",
                "3", // conflict (can't tell, is it was deleted/deleted + inserted new, or just modified/deleted
                "Four"
            };

            var changedAnother = new List<string> {
                "1", //conflict
                "    Two;", //not conflict
                "Four"
            };


            _expectedText = new List<string> {
                "//// Conflict ////",
                "//// source.txt:",
                "One",
                "//// one changed file:",
                "One;",
                "//// another changed file:",
                "1",
                "//// End of conflict ////",
                "    Two; ",
                "//// Conflict ////",
                "//// source.txt:",
                "Three",
                "//// one changed file:",
                "3",
                "//// another changed file:",
                "//// line was deleted",
                "//// End of conflict ////",
                "Four"
            };


            _ProcessingFiles.ChangedFiles.Add(new TextFile("one changed file", changedOne));
            _ProcessingFiles.ChangedFiles.Add(new TextFile("another changed file", changedAnother));
        }




        protected override void Because_of() {
            base.Because_of();
            _mergeResult = _Merger.Merge(_ProcessingFiles);
        }

        [Fact] public void Merged_Text_Should_Contain_Only_All_Changes_In_Right_Order() {
            _mergeResult.Output.Text.ShouldEqual(_expectedText);
        }


        [Fact] public void Resulted_Filename_Should_Equals_To_Source_Filename() { // todo: maybe this need to be changed?
            _mergeResult.Output.Filename.ShouldEqual(_ProcessingFiles.SourceFile.Filename); 
        }
        
    }
}
