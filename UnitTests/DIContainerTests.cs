﻿using NBehave.Spec.Xunit;
using SimpleAutoMerge.Abstract.Services;
using SimpleAutoMerge.Services;
using Xunit;

// ReSharper disable once CheckNamespace
namespace SimpleAutoMerge.When_Using_DI_Container
{
    public class When_Using_DI_Container : Specification
    {
        protected IDIContainer _Container;

        protected override void Establish_context() {
            base.Establish_context();
            _Container = DIFactory.GetContainer();
        }
    }


    public class And_Getting_Stuff_From_It : When_Using_DI_Container
    {
        [Fact] public void Should_Be_Abble_To_Get_IArgsInterpreterFactory() {
            _Container.Get<IArgsInterpreterFactory>().ShouldNotBeNull();
        }

        [Fact] public void Should_Be_Abble_To_Get_IFileProcessor() {
            _Container.Get<IFileProcessor>().ShouldNotBeNull();
        }

        [Fact] public void Should_Be_Abble_To_Get_ITextMerger() {
            _Container.Get<ITextMerger>().ShouldNotBeNull();
        }
    }
}
